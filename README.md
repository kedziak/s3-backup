# S3::Backup

This gem provides module and rake task to backup/restore postgresql db to/from AWS s3 service.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 's3-backup'
```

And then execute:

    $ bundle exec backupit

This will create './config/s3.yml'. Provide s3 credentials here.

```
development:
  AWS_REGION:           ''
  AWS_ACCESS_KEY_ID:    ''
  AWS_SECRET_KEY:       ''
  backup_bucket_name:   ''
  uploads_bucket_name:  ''

staging:
  AWS_REGION:           ''
  AWS_ACCESS_KEY_ID:    ''
  AWS_SECRET_KEY:       ''
  backup_bucket_name:   ''
  assets_bucket_name:   ''
  uploads_bucket_name:  ''

production:
  AWS_REGION:           ''
  AWS_ACCESS_KEY_ID:    ''
  AWS_SECRET_KEY:       ''
  backup_bucket_name:   ''
  assets_bucket_name:   ''
  uploads_bucket_name:  ''
```

## Usage
For backup:

    $ rake db:backup

For restore:

    $ rake db:restore
