require 's3/backup'

desc 'Backup and restore db to/from amazon S3'
namespace :db do
  task backup: :environment do
    # dump the backup and zip it up
    print "creating #{dump_file}..."
    # create directory if not exest
    system "pg_dump -Fc --no-owner --dbname=#{db_name} | gzip -c > #{dump_file}"
    puts 'done'

    print "uploading #{dump_file} to Amazon S3.."
    s3 = S3::Backup::Connector.new
    s3.send(dump_file)
    puts 'done'

    puts 'deleting dumped file.'
    File.delete(dump_file)
  end

  task restore: :environment do
    print "downloading latest dump from Amazon S3 to #{dump_file}..."
    s3 = S3::Backup::Connector.new
    s3.retrieve_latest(dump_file)
    puts 'done'

    print "unzip and restore db from #{dump_file}..."
    system "gunzip -c #{dump_file} | pg_restore --clean --no-owner --dbname=#{db_name}"
    puts 'done'

    print "deleting #{dump_file}..."
    File.delete(dump_file)
    puts 'done'
  end

  private

  def dump_file
    # before compress drop it in the tmp/backups
    Rails.root.join('tmp', 'dump.psql.gz')
  end

  def db_name
    config = Rails.configuration.database_configuration[Rails.env]
    "postgresql://#{config['username']}:#{config['password']}@127.0.0.1:5432/#{config['database']}"
  end
end
