module S3
  module Backup
    class Railtie < Rails::Railtie
      rake_tasks do
        load 'tasks/backup.rake'
      end
    end
  end
end
