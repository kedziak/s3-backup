lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 's3/backup/version'

Gem::Specification.new do |spec|
  spec.name          = 's3-backup'
  spec.version       = S3::Backup::VERSION
  spec.authors       = ['Valiantsin Mikhaliuk', 'Piotr Kedziak']
  spec.email         = ['valiantsin.mikhaliuk@gmail.com', 'piotr@kedziak.com']

  spec.summary       = 'Module for upload postgres db backups to s3'
  spec.homepage      = 'https://bitbucket.org/kedziak/s3-backup'

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir = 'bin'
  spec.executables << 'backupit'
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 1.15'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_dependency 'aws-sdk', '~> 2'
  spec.add_dependency 'zip'
end
